using Microsoft.EntityFrameworkCore;
using Parking.API.Models;

namespace Parking.API.Data.Contexts
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {}

        public DbSet<ParkingArea> ParkingAreas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ParkingArea>()
          .Property(pa => pa.WeekdayRate)
          .HasPrecision(18, 2);

            modelBuilder.Entity<ParkingArea>()
                .Property(pa => pa.WeekendRate)
                .HasPrecision(18, 2);

            modelBuilder.Entity<ParkingArea>().HasKey(pa => pa.Id);
        }
    }
}