using Microsoft.EntityFrameworkCore;
using Parking.API.Data.Contexts;
using Parking.API.Models;

namespace Parking.API.Data.Repositories
{
    public class ParkingAreaRepository : IParkingAreaRepository
    {
        private readonly ApplicationDbContext _context;

        public ParkingAreaRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ParkingArea>> GetAllAsync()
        {
            return await _context.ParkingAreas.ToListAsync();
        }

        public async Task<ParkingArea?> GetByIdAsync(int id)
        {
            return await _context.ParkingAreas.FindAsync(id);
        }

        public async Task AddAsync(ParkingArea parkingArea)
        {
            await _context.ParkingAreas.AddAsync(parkingArea);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(ParkingArea parkingArea)
        {
            _context.Entry(parkingArea).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var parkingArea = await GetByIdAsync(id);
            if (parkingArea != null)
            {
                _context.ParkingAreas.Remove(parkingArea);
                await _context.SaveChangesAsync();
            }
        }
    }
}