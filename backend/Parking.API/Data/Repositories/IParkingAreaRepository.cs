using Parking.API.Models;

namespace Parking.API.Data.Repositories
{
    public interface IParkingAreaRepository
    {
        Task<IEnumerable<ParkingArea>> GetAllAsync();
        Task<ParkingArea?> GetByIdAsync(int id);
        Task AddAsync(ParkingArea parkingArea);
        Task UpdateAsync(ParkingArea parkingArea);
        Task DeleteAsync(int id);
    }
}