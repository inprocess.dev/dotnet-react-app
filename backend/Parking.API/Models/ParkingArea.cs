namespace Parking.API.Models
{
    public class ParkingArea
    {
        public int Id { get; set; }
        public string ParkingAreaName { get; set; } = String.Empty;
        public decimal WeekdayRate { get; set; }
        public decimal WeekendRate { get; set; }
        public int DiscountPercentage { get; set; }
    }
}