using Microsoft.AspNetCore.Mvc;
using Parking.API.Data.Repositories;
using Parking.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Parking.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingAreasController : ControllerBase
    {
        private readonly IParkingAreaRepository _repository;

        public ParkingAreasController(IParkingAreaRepository repository)
        {
            _repository = repository;
        }

        // GET: api/ParkingAreas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ParkingArea>>> GetParkingAreas()
        {
            return Ok(await _repository.GetAllAsync());
        }

        // POST: api/ParkingAreas
        [HttpPost]
        public async Task<ActionResult<ParkingArea>> PostParkingArea(ParkingArea parkingArea)
        {
            await _repository.AddAsync(parkingArea);
            return CreatedAtAction(nameof(GetParkingArea), new { id = parkingArea.Id }, parkingArea);
        }

        // GET: api/ParkingAreas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ParkingArea>> GetParkingArea(int id)
        {
            var parkingArea = await _repository.GetByIdAsync(id);

            if (parkingArea == null)
            {
                return NotFound();
            }

            return parkingArea;
        }

        // PUT: api/ParkingAreas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParkingArea(int id, ParkingArea parkingArea)
        {
            if (id != parkingArea.Id)
            {
                return BadRequest();
            }

            try
            {
                await _repository.UpdateAsync(parkingArea);
                return NoContent();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await ParkingAreaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        // DELETE: api/ParkingAreas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteParkingArea(int id)
        {
            var parkingArea = await _repository.GetByIdAsync(id);
            if (parkingArea == null)
            {
                return NotFound();
            }

            await _repository.DeleteAsync(id);
            return NoContent();
        }

        private async Task<bool> ParkingAreaExists(int id)
        {
            return await _repository.GetByIdAsync(id) != null;
        }
    }
}