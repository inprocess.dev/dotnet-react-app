import { useState, useEffect, useCallback } from "react";
import { toast } from "react-toastify";
import { API_BASE_URL } from "../constants";
import { ParkingArea } from "../types/parkingArea";
import { NewParkingArea } from "../types/newParkingArea";

export function useParkingAreas() {
  const [parkingAreas, setParkingAreas] = useState<ParkingArea[]>([]);
  const [availableParkingAreas, setAvailableParkingAreas] = useState<ParkingArea[]>([]);

  const fetchParkingAreas = async () => {
    try {
      const response = await fetch(`${API_BASE_URL}/ParkingAreas`);
      if (response.ok) {
        const data: ParkingArea[] = await response.json();
        setParkingAreas(data);
      } else {
        toast.error(`Failed to load parking areas: ${response.status}`);
      }
    } catch (error) {
      console.error(
        "An error occurred while fetching the parking areas:",
        error
      );
      toast.error("An error occurred while fetching the parking areas.");
    }
  };

  const addParkingArea = async (newArea: NewParkingArea) => {
    try {
      const response = await fetch(`${API_BASE_URL}/ParkingAreas`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newArea),
      });

      if (response.ok) {
        const addedArea: ParkingArea = await response.json();
        setParkingAreas((prevAreas) => [...prevAreas, addedArea]);
        toast.success("Parking area added successfully.");
      } else {
        toast.error("Failed to add parking area.");
      }
    } catch (error) {
      console.error("An error occurred while adding the parking area:", error);
      toast.error("An error occurred while adding the parking area.");
    }
  };

  const updateParkingArea = async (updatedArea: ParkingArea) => {
    try {
      const response = await fetch(
        `${API_BASE_URL}/ParkingAreas/${updatedArea.id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(updatedArea),
        }
      );

      if (response.ok) {
        setParkingAreas((prevAreas) =>
          prevAreas.map((area) =>
            area.id === updatedArea.id ? updatedArea : area
          )
        );
        toast.success("Parking area updated successfully.");
      } else {
        toast.error("Failed to update parking area.");
      }
    } catch (error) {
      console.error(
        "An error occurred while updating the parking area:",
        error
      );
      toast.error("An error occurred while updating the parking area.");
    }
  };

  const deleteParkingArea = async (id: string) => {
    if (window.confirm("Are you sure you want to delete this parking area?")) {
      try {
        const response = await fetch(`${API_BASE_URL}/ParkingAreas/${id}`, {
          method: "DELETE",
        });

        if (response.ok) {
          toast.success("Parking area deleted successfully.");
          setParkingAreas((prevParkingAreas) =>
            prevParkingAreas.filter((area) => area.id !== id)
          );
        } else {
          toast.error("Failed to delete parking area.");
        }
      } catch (error) {
        console.error(
          "An error occurred while deleting the parking area:",
          error
        );
        toast.error("An error occurred while deleting the parking area.");
      }
    }
  };

  const fetchAvailableParkingAreas = useCallback(async () => {
    try {
      const response = await fetch(`${API_BASE_URL}/ParkingAreas/Available`);
      if (response.ok) {
        const data: ParkingArea[] = await response.json();
        setAvailableParkingAreas(data);
      } else {
        toast.error(`Failed to load available parking areas: ${response.status}`);
      }
    } catch (error) {
      console.error(
        "An error occurred while fetching the available parking areas:",
        error
      );
      toast.error("An error occurred while fetching the available parking areas.");
    }
  }, []);

  useEffect(() => {
    fetchParkingAreas();
  }, []);

  return {
    parkingAreas,
    availableParkingAreas,
    fetchAvailableParkingAreas,
    addParkingArea,
    updateParkingArea,
    deleteParkingArea,
  };
}