import { createContext } from 'react';
import { ParkingArea } from '../types/parkingArea';

interface ParkingAreaContextType {
  deleteParkingArea: (id: string) => Promise<void>;
  updateParkingArea: (area: ParkingArea) => Promise<void>;
}

export const ParkingAreaContext = createContext<ParkingAreaContextType | undefined>(undefined);