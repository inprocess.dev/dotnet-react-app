import React from 'react';
import { Link } from 'react-router-dom';
import styles from './navigation.module.css';

const Navigation: React.FC = () => {
  return (
    <nav className={styles.nav}>
      <Link to="/" className={styles.navLink}>Home</Link>
      <Link to="/parking" className={styles.navLink}>Manage Parking Areas</Link>
      <Link to="/payment" className={styles.navLink}>Calculate Parking Fee</Link>
    </nav>
  );
};

export default Navigation;