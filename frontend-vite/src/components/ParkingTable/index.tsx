import React from 'react';
import ParkingTableRow from '../ParkingTableRow';
import styles from './parking-table.module.css';
import { ParkingArea } from '../../types/parkingArea';

interface ParkingTableProps {
  parkingAreas: ParkingArea[];
}

const ParkingTable: React.FC<ParkingTableProps> = ({ parkingAreas }) => {
  return (
    <table className={styles.parkingTable}>
      <thead className={styles.tableHeader}>
        <tr>
          <th className={styles.headerCell}>Parking Area Name</th>
          <th className={styles.headerCell}>Weekdays Hourly Rate (USD)</th>
          <th className={styles.headerCell}>Weekend Hourly Rate (USD)</th>
          <th className={styles.headerCell}>Discount Percentage (%)</th>
          <th className={styles.headerCell}>Actions</th>
        </tr>
      </thead>
      <tbody>
        {parkingAreas.map((area) => (
          <ParkingTableRow key={area.id} area={area} />
        ))}
      </tbody>
    </table>
  );
};

export default ParkingTable;