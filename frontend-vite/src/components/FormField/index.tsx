import React, { ChangeEventHandler } from 'react';
import styles from './form-field.module.css';

interface FormFieldProps {
  label: string;
  type: string;
  id: string;
  name: string;
  step?: string;
  min?: string;
  max?: string;
  required?: boolean;
  value?: string | number;
  onChange?: ChangeEventHandler<HTMLInputElement>;
}

const FormField: React.FC<FormFieldProps> = ({
  label, type, id, name, step, min, max, required, value, onChange
}) => {
  return (
    <div className={styles.formField}>
      <label htmlFor={id} className={styles.label}>{label}</label>
      {onChange ? (
        <input
          type={type}
          id={id}
          name={name}
          step={step}
          min={min}
          max={max}
          required={required}
          value={value}
          onChange={onChange}
          className={styles.input}
        />
      ) : (
        <input
          type={type}
          id={id}
          name={name}
          step={step}
          min={min}
          max={max}
          required={required}
          defaultValue={value}
          className={styles.input}
        />
      )}
    </div>
  );
}

export default FormField;