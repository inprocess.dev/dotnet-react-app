import React, { useContext, useState, ChangeEvent } from "react";
import { ParkingAreaContext } from "../../contexts/ParkingAreaContext";
import ActionButton from "../ActionButton";
import FormField from "../FormField";
import styles from "./parking-table-row.module.css";
import { ParkingArea } from "../../types/parkingArea";

interface ParkingTableRowProps {
  area: ParkingArea;
}

const ParkingTableRow: React.FC<ParkingTableRowProps> = ({ area }) => {

  const context = useContext(ParkingAreaContext);
  if (!context) { throw new Error('ParkingTableRow must be used within a ParkingAreaContext.Provider'); }
  const { deleteParkingArea, updateParkingArea } = context;

  const [isEditing, setIsEditing] = useState(false);
  const [editedArea, setEditedArea] = useState<ParkingArea>({ ...area });

  const handleSave = async () => {
    await updateParkingArea(editedArea);
    setIsEditing(false);
  };

  const handleCancel = () => {
    setIsEditing(false);
    setEditedArea({ ...area });
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setEditedArea((prev) => ({ ...prev, [name]: value }));
  };

  return (
    <tr className={styles.row}>
      {isEditing ? (
        <>
          <td className={styles.cell}>
            <FormField label="" type="text" id={`name-${area.id}`} name="parkingAreaName" value={editedArea.parkingAreaName} onChange={handleChange} required />
          </td>
          <td className={styles.cell}>
            <FormField label="" type="number" id={`weekdayRate-${area.id}`} name="weekdayRate" value={editedArea.weekdayRate} onChange={handleChange} step="0.01" min="0" required />
          </td>
          <td className={styles.cell}>
            <FormField label="" type="number" id={`weekendRate-${area.id}`} name="weekendRate" value={editedArea.weekendRate} onChange={handleChange} step="0.01" min="0" required />
          </td>
          <td className={styles.cell}>
            <FormField label="" type="number" id={`discount-${area.id}`} name="discountPercentage" value={editedArea.discountPercentage} onChange={handleChange} step="1" min="0" max="100" required />
          </td>
          <td className={styles.cell}>
            <ActionButton actionType="edit" onClick={handleSave}>
              Save
            </ActionButton>
            <ActionButton
              actionType="edit"
              onClick={handleCancel}
            >
              Cancel
            </ActionButton>
          </td>
        </>
      ) : (
        <>
          <td className={styles.cell}>{area.parkingAreaName}</td>
          <td className={styles.cell}>{area.weekdayRate}</td>
          <td className={styles.cell}>{area.weekendRate}</td>
          <td className={styles.cell}>{area.discountPercentage}%</td>
          <td className={styles.cell}>
            <ActionButton actionType="edit" onClick={() => setIsEditing(true)}>
              Edit
            </ActionButton>
            <ActionButton
              actionType="delete"
              onClick={() => deleteParkingArea(area.id)}
            >
              Delete
            </ActionButton>
          </td>
        </>
      )}
    </tr>
  );
}

export default ParkingTableRow;