import React, { useState, useEffect } from 'react';
import { useParkingAreas } from '../../hooks/useParkingAreas';
import FormField from '../FormField';
import SelectField from '../SelectField';
import SubmitButton from '../SubmitButton';
import { getCurrentTime, getCurrentDay } from '../../utils';
import styles from './payment-calculator-form.module.css';

const PaymentCalculatorForm: React.FC = () => {

    const currentDay = getCurrentDay();
    const currentTime = getCurrentTime();

    const [selectedParkingArea, setSelectedParkingArea] = useState('');
    const [currency, setCurrency] = useState('USD');

    const { availableParkingAreas, fetchAvailableParkingAreas } = useParkingAreas();

    useEffect(() => {
      fetchAvailableParkingAreas();
    }, [fetchAvailableParkingAreas]);
  

    const parkingAreaOptions = availableParkingAreas.map(area => ({
        value: area.id,
        text: `${area.parkingAreaName} - from $${area.weekdayRate.toFixed(2)}/hr`,
    }));

    const currencyOptions = [
        { value: 'USD', text: 'USD' },
        { value: 'EUR', text: 'EUR' },
        { value: 'PLN', text: 'PLN' },
    ];

    const handleParkingAreaChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setSelectedParkingArea(event.target.value);
    };

    const handleCurrencyChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setCurrency(event.target.value);
    };

    return (
        <form id="paymentCalculatorForm" action="/payment/calculate" className={styles.formContainer}>
            <SelectField
                id="selectedParkingArea"
                label="Select Parking Area:"
                name="selectedParkingArea"
                required
                selectedValue={selectedParkingArea}
                onChange={handleParkingAreaChange}
                options={parkingAreaOptions}
            />

            <FormField label="Start Time:" type="time" id="startTime" name="startTime" value={currentTime} required />
            <FormField label="End Time:" type="time" id="endTime" name="endTime" required />
            <FormField label="Parking Day:" type="date" id="parkingDay" name="parkingDay" value={currentDay} required />

            <SelectField
                id="currency"
                label="Currency:"
                name="currency"
                required
                selectedValue={currency}
                onChange={handleCurrencyChange}
                options={currencyOptions}
            />

            <SubmitButton id="calculatePayment" value="Calculate" />
        </form>
    );
};

export default PaymentCalculatorForm;