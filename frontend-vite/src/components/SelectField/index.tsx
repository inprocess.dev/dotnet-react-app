import React, { ChangeEventHandler } from 'react';
import styles from './select-field.module.css';

interface OptionItem {
  value: string;
  text: string;
}

interface SelectFieldProps {
  id: string;
  label: string;
  name: string;
  required: boolean;
  selectedValue: string;
  onChange?: ChangeEventHandler<HTMLSelectElement>;
  options: OptionItem[];
}

const SelectField: React.FC<SelectFieldProps> = ({
  id, label, name, required, selectedValue, onChange, options
}) => {
  return (
    <div className={styles.selectFieldContainer}>
      <label htmlFor={id} className={styles.selectLabel}>{label}</label>
      <select
        id={id}
        name={name}
        required={required}
        value={selectedValue}
        onChange={onChange}
        className={styles.selectField}
      >
        {options.map(option => (
          <option key={option.value} value={option.value}>
            {option.text}
          </option>
        ))}
      </select>
    </div>
  );
};

export default SelectField;