import React, { FormEvent } from "react";
import FormField from "../FormField";
import SubmitButton from "../SubmitButton";
import styles from "./form-container.module.css";
import { NewParkingArea } from "../../types/newParkingArea";

interface FormContainerProps {
  onSubmit: (newArea: NewParkingArea) => void;
  formId: string;
}

const FormContainer: React.FC<FormContainerProps> = ({ onSubmit, formId }) => {

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const values: NewParkingArea = {
      parkingAreaName: formData.get("parkingAreaName") as string,
      weekdayRate: Number(formData.get("weekdayRate")) || 0,
      weekendRate: Number(formData.get("weekendRate")) || 0,
      discountPercentage: Number(formData.get("discount")) || 0,
    };

    onSubmit(values);
  };

  return (
    <form id={formId} onSubmit={handleSubmit} className={styles.formContainer}>
     <FormField
        label="Parking Area Name:"
        type="text"
        id="parkingAreaName"
        name="parkingAreaName"
        value=""
        required
      />
      <FormField
        label="Weekdays Hourly Rate (USD):"
        type="number"
        id="weekdayRate"
        name="weekdayRate"
        step="0.01"
        min="0"
        value=""
        required
      />
      <FormField
        label="Weekend Hourly Rate (USD):"
        type="number"
        id="weekendRate"
        name="weekendRate"
        step="0.01"
        min="0"
        value=""
        required
      />
      <FormField
        label="Discount Percentage (%):"
        type="number"
        id="discount"
        name="discount"
        step="1"
        min="0"
        max="100"
        value=""
        required
      />
      <SubmitButton id="submitForm" value="Submit" />
    </form>
  );
};

export default FormContainer;