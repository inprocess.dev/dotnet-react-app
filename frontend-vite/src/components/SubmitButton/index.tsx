import React from 'react';
import styles from './submit-button.module.css';

interface SubmitButtonProps {
  id: string;
  value: string;
}

const SubmitButton: React.FC<SubmitButtonProps> = ({ id, value }) => {
  return (
    <button type="submit" id={id} className={styles.submitButton}>
      {value}
    </button>
  );
};

export default SubmitButton;