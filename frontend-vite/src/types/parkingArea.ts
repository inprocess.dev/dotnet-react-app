export interface ParkingArea {
  id: string;
  parkingAreaName: string;
  weekdayRate: number;
  weekendRate: number;
  discountPercentage: number;
}