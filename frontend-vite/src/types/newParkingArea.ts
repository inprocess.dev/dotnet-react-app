
export interface NewParkingArea {
    parkingAreaName: string;
    weekdayRate: number;
    weekendRate: number;
    discountPercentage: number;
  }