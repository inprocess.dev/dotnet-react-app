import React, { useEffect } from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ParkingAreaContext } from '../../contexts/ParkingAreaContext';
import { useAppContext } from '../../contexts/AppContext';
import FormContainer from '../../components/FormContainer';
import PageHeader from '../../components/PageHeader';
import ParkingTable from '../../components/ParkingTable';
import { useParkingAreas } from '../../hooks/useParkingAreas';
import { ParkingArea } from '../../types/parkingArea';


const ManageParkingPage: React.FC = () => {
  const { setHeaderText } = useAppContext();
  const { parkingAreas, addParkingArea, updateParkingArea, deleteParkingArea } = useParkingAreas();

  useEffect(() => {
    setHeaderText('Manage Parking Page');
  }, [setHeaderText]);

  return (
    <div className='content'>
      <ParkingAreaContext.Provider value={{ deleteParkingArea, updateParkingArea }}>
        <ToastContainer />
        <PageHeader text='Add Parking Area' />
        <FormContainer
          onSubmit={addParkingArea}
          formId='parkingAreaForm'
        />
        <ParkingTable parkingAreas={parkingAreas as ParkingArea[]} />
      </ParkingAreaContext.Provider>
    </div>
  );
};

export default ManageParkingPage;