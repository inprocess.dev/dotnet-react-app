import React, { useEffect } from 'react';
import { useAppContext } from '../../contexts/AppContext';

const HomePage: React.FC = () => {
    const { setHeaderText } = useAppContext();

    useEffect(() => {
        setHeaderText('Home');
    }, [setHeaderText]);

    return <div className='content'>Welcome to the Parking Lot Management System. Please choose an option from the menu to get started.</div>;
}

export default HomePage;